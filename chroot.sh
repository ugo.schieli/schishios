#!/bin/bash

set -euxo pipefail


echo 'Begin of the Chroot Script'

# Time zone
echo 'Time Zone Configuration'
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc

# Localization
echo 'Localization Configuration'
echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' >> /etc/locale.conf
echo 'KEYMAP=fr-latin9' >> /etc/vconsole.conf

# Network configuration
echo 'Network Configuration'
echo 'gemini' >> /etc/hostname
systemctl enable NetworkManager.service
cat <<EOF > /etc/hosts
127.0.0.1        localhost
::1              localhost
127.0.1.1        gemini
EOF

# Bluetooth Configuration
systemctl enable bluetooth.service

# Root password
echo 'Set Root password'
passwd

# User configuration
echo 'Create ugo users'
useradd -m -G adm ftp games http log rfkill sys systemd-journal uucp wheel ugo
echo 'Set password for ugo'
passwd ugo
echo 'Enable sudo for wheel group'
echo '%wheel      ALL=(ALL:ALL) ALL' >> /etc/sudoers

# Pacman Configuration
sed -i '/VerbosePkgLists/s/^#//g' /etc/pacman.conf
sed -i '/Color/s/^#//g' /etc/pacman.conf
sed -i '/ParallelDownloads = 5/s/^#//g' /etc/pacman.conf
sed -i '93s/^#//g' /etc/pacman.conf
sed -i '94s/^#//g' /etc/pacman.conf

# Systemd-boot Installation
echo "Bootloader Installation"
bootctl install
cp -v /schischios/boot/loader.conf /boot/loader/loader.conf
cp -v /schischios/boot/arch.conf /boot/loader/entries/arch.conf

# Mirror Configuration
cat <<EOF > /etc/xdg/reflector/reflector.conf
--save /etc/pacman.d/mirrorlist
--country France,Germany
--protocol https
--latest 10
--sort rate
EOF

systemctl enable reflector.service


# XOrg Configuration
echo "Keyboard Configuration"
echo "Touchpad Configuration"
cp -v /schischios/xorg.conf.d/* /etc/X11/xorg.conf.d/

systemctl enable lxdm.service
systemctl enable fstrim.timer
systemctl enable acpid.service

cp -v /schischios/acpi/* /etc/acpi/
chmod +x /etc/acpi/handlers/*

# Dotfiles Installation
cp -v /schischios/.config/* /home/ugo/.config/
