#!/bin/bash

set -euxo pipefail

DISK=/dev/nvme0n1
EFI_PART=/dev/nvme0n1p1
SWAP_PART=/dev/nvme0n1p2
ROOT_PART=/dev/nvme0n1p3

timedatectl set-ntp true

parted $DISK mklabel gpt
parted $DISK mkpart "EFI_system_partition" fat32 1MiB 301MiB
parted $DISK set 1 esp on
parted $DISK mkpart "Swap_partition" linux-swap 301MiB 8493MiB
parted $DISK mkpart "Root_partition" ext4 8493MiB 100%
parted $DISK print

sleep 3s

mkfs.fat -F 32 $EFI_PART
mkswap -L swap_partion $SWAP_PART
mkfs.ext4 -L root_partition $ROOT_PART

sleep 3s

mount -v $ROOT_PART /mnt
mkdir -v /mnt/boot
mount -v $EFI_PART /mnt/boot
swapon $SWAP_PART

sleep 3s

pacstrap /mnt - < ./packages/base.txt
pacstrap /mnt - < ./packages/video-drivers.txt
pacstrap /mnt - < ./packages/developpment.txt
pacstrap /mnt - < ./packages/libraries.txt
pacstrap /mnt - < ./packages/audio.txt
pacstrap /mnt - < ./packages/fonts.txt
pacstrap /mnt - < ./packages/wine.txt
pacstrap /mnt - < ./packages/desktop-environnement.txt

genfstab -U /mnt >> /mnt/etc/fstab

cp -rv . /mnt/schischios

chmod +x /mnt/schischios/chroot.sh
arch-chroot /mnt /schischios/chroot.sh

echo 'Installation finished'
