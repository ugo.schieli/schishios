import XMonad
import XMonad.Config.Azerty
import XMonad.Hooks.EwmhDesktops

main :: IO ()
main = xmonad $ ewmh $ myConfig

myConfig = azertyConfig
	{ modMask = mod4Mask
	, terminal = "alacritty"
	}
