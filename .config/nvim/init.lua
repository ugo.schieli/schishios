local impatient_ok, impatient = pcall(require, 'impatient')
if impatient_ok then impatient.enable_profile() end

local sources = {
	"plugins",
	"keymaps",
	"colorscheme",
	"treesitter",
	"options",
	"lualine",
	"bufferline",
	"colorizer",
	"tree",
	"gitsigns",
	"lsp",
	"cmp",
	"which_key",
	"autopairs",
	--"illuminate",
}

for _, source in pairs(sources) do
	local status_ok, error = pcall(require, 'user.' .. source)
	if not status_ok then
		vim.api.nvim_err_writeln("Error while loading " .. source .. "\n\n" .. error)
	end
end
