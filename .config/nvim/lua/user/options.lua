local opt = vim.opt

opt.autoindent = true
opt.copyindent = true
opt.clipboard = "unnamed"
opt.mouse = "a"
opt.number = true
opt.relativenumber = true
opt.cursorline = false
opt.smartindent = true
opt.smartcase = true
opt.tabstop = 4
opt.shiftwidth = 4
opt.termguicolors = true
