local mason = require "user.lsp.mason"

local status_ok, lspconfig = pcall(require, 'lspconfig')
if not status_ok then
	return
end

local server_list = mason.server_list

for _, server in pairs(server_list) do
	local opts = {
		on_attach = require 'user.lsp.handlers'.on_attach,
		capabilities = require 'user.lsp.handlers'.capabilities,
	}

	local has_custom_opts, server_custom_opts = pcall(require, 'user.lsp.settings.' .. server)
	if has_custom_opts then
		opts = vim.tbl_deep_extend("force", opts, server_custom_opts)
	end

	lspconfig[server].setup(opts)
end
