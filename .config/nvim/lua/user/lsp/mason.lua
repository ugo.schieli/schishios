local M = {}

local status_ok_mason, mason = pcall(require, 'mason')
if not status_ok_mason then
	return
end

local status_ok_config, mason_lspconfig = pcall(require, 'mason-lspconfig')
if not status_ok_config then
	return
end

local server_list = {
	"sumneko_lua",
	"pyright",
	"bashls",
	"clangd",
	"cmake",
	"asm_lsp",
	"cssls",
	"dockerls",
	"gopls",
	"html",
	"hls",
	"jsonls",
	"jdtls",
	"quick_lint_js",
	"ltex",
	"marksman",
	"taplo",
	"yamlls",
	"lemminx",
	"vimls",
}

mason.setup()

mason_lspconfig.setup({
	ensure_installed = server_list,
	automatic_installation = false,
})

M.server_list = server_list

return M
