local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
	return
end

configs.setup({
  ensure_installed = {
    "lua",
    "python",
    "rust",
    "c",
    "cpp",
    "bash",
    "yaml",
    "toml",
  },
  highlight = {
    enable = true,
  },
  indent = {
    enable = true,
  },
})

vim.opt.foldlevel = 99
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
